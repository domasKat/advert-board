# AdvertBoard installation guide

## REQUIREMENTS

The minimum requirements for this application template is PHP 7.1.0 and MySQL 5.6.

## INSTALLATION

1. Clone the project from: https://gitlab.com/domasKat/advert-board.git
2. Install dependencies by running `composer install` 
3. Set up environment variables in `.env` file
4. Create database by running `php bin/console doctrine:database:create`
5. Create required tables by running `php bin/console doctrine:migrations:migrate`
6. Install Webpack Encore to the project by running `yarn add @symfony/webpack-encore --dev`
7. Run `yarn run encore dev`
8. Start the server and access the project. (localhost:8000 if locally)

##IMPORTANT

Disable AdBlock in browser if you can`t see css styles after deploy or README.md in gitlab repository.
