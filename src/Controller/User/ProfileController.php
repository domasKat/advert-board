<?php


namespace App\Controller\User;


use App\Entity\Advert;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{


    /**
     * @Route("/profile", name="profile")
     */
    public function provider()
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $adverts = $em->getRepository(Advert::class)->findBy(['user' => $user->getId()]);
;
        return $this->render('user/profile.html.twig', [
            'adverts' => $adverts
        ]);
    }
}