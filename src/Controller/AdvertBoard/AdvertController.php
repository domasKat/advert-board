<?php


namespace App\Controller\AdvertBoard;


use App\Entity\Advert;
use App\Form\AdvertForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class AdvertController extends AbstractController
{

    /**
     * @Route("/advert/create", name="advert_create")
     */
    public function create(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $advert = new Advert();
        $form = $this->createForm(AdvertForm::class, $advert);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $advert->setUserId($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($advert);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render("advert/create.html.twig", [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/advert/update/{id}", name="advert_update")
     */
    public function update(Request $request, $id)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $em = $this->getDoctrine()->getManager();
        $advert = $em->getRepository(Advert::class)->findOneBy(['id' => $id]);

        try {

            if($advert instanceof Advert) {

                $form = $this->createForm(AdvertForm::class, $advert);
                $form->handleRequest($request);

                if($form->isSubmitted() && $form->isValid()) {

                    $advert->setUpdatedAt(new \DateTime('now'));

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($advert);
                    $em->flush();

                    return $this->redirectToRoute('profile');
                }

                return $this->render("advert/update.html.twig", [
                    'form' => $form->createView()
                ]);

            }

            throw new NotFoundResourceException("Advert Not found");

        } catch (NotFoundResourceException $e) {

            throw $e;

        }
    }

    /**
     * @Route("/advert/delete/{id}", name="advert_delete")
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete($id)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $em = $this->getDoctrine()->getManager();
        $advert = $em->getRepository(Advert::class)->findOneBy(['id' => $id]);

        try {

            if($advert instanceof Advert) {

                $em->remove($advert);
                $em->flush();

                return $this->redirectToRoute("profile");
            }

            throw new NotFoundResourceException("Advert Not found");

        } catch (NotFoundResourceException $e) {

            throw $e;

        }

    }
}