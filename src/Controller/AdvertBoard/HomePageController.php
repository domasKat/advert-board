<?php

namespace App\Controller\AdvertBoard;

use App\Entity\Advert;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {

        $doctrine = $this->getDoctrine();
        $adverts = $doctrine->getRepository(Advert::class)->findAdvertsWithUser();

        return $this->render('homepage/index.html.twig', [
            'adverts' => $adverts
        ]);
    }

}