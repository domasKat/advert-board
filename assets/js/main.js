var $ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('../css/global.scss');
require('bootstrap');
require('bootstrap');

// or you can include specific pieces
// require('bootstrap-sass/javascripts/bootstrap/navbar');
// require('bootstrap-sass/javascripts/bootstrap/popover');

$(document).ready(function() {

});